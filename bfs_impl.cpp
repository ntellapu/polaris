// Including 'hpx/hpx_main.hpp' instead of the usual 'hpx/hpx_init.hpp' enables
// to use the plain C-main below as the direct main HPX entry point.
#include <hpx/future.hpp>
#include <hpx/hpx_main.hpp>
#include <hpx/iostream.hpp>
#include "libhungarian/hungarian.c"
#include "libhungarian/hungarian.hpp"
#include "qap_common.hpp"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <istream>
#include <limits>
#include <numeric>
#include <ostream>
#include <string>
#include <vector>

class qap_task
{
public:
    std::vector<int> p_;
    int cost;
};

class step_exec
{
public:
    int level;
    inline static int n_;                 // number of facilities/locations
    inline static std::vector<int> F_;    // flow matrix
    inline static std::vector<int> D_;    // distance matrix
    
    std::vector<hpx::future<qap_task>> tasks_in_step;

    step_exec(int i) : level(i) {};

    static int compute_cost(const std::vector<int>& p, int k = n_) {
        int Z = 0;

        for (int i = 0; i < k; ++i)
        {
            for (int j = 0; j < k; ++j)
                Z += F_[i * n_ + j] * D_[p[i] * n_ + p[j]];
        }    // for i

        return Z;
    } // compute_cost

    static std::vector<int> ordered_product(std::vector<int>& F, std::vector<int>& D, int m) {
        std::vector<int> OP(m * m, 0);

        for (auto iter = std::begin(F), end = std::end(F); iter < end;
             iter += m)
        {
            std::sort(iter, iter + m);
        }

        // D must be transposed
        for (auto iter = std::begin(D), end = std::end(D); iter < end;
             iter += m)
        {
            std::sort(iter, iter + m, std::greater<void>());
        }

        for (int i = 0; i < m; ++i)
        {
            for (int j = 0; j < m; ++j)
            {
                // we avoid diagonal elements
                for (int k = 1; k < m; ++k)
                    OP[i * m + j] += (F[i * m + k] * D[j * m + k - 1]);
            }
        }

        return OP;
    }    // ordered_product

    static int compute_lower_bound(const std::vector<int>& p, int k)
    {
        auto alpha = p.data();       // k finished assignments
        auto beta = p.data() + k;    // m assignments to make

        int m = n_ - k;

        // construct linear assignment matrix (Eq. 4)
        std::vector<int> B(m * m, 0);

        // remaining assignments
        std::vector<int> Fp(m * m, 0);
        std::vector<int> Dp(m * m, 0);    // Dp is transposed

        for (int i = 0; i < m; ++i)
        {
            for (int j = 0; j < m; ++j)
                Fp[i * m + j] = F_[(k + i) * n_ + k + j];
        }

        for (int i = 0; i < m; ++i)
        {
            for (int j = 0; j < m; ++j)
                Dp[i * m + j] = D_[beta[j] * n_ + beta[i]];
        }

        auto OP = ordered_product(Fp, Dp, m);

        // assignments made
        for (int i = 0; i < m; ++i)
        {
            for (int bi = 0; bi < m; ++bi)
            {
                int b = 0;
                for (int j = 0; j < k; ++j)
                {
                    b += (F_[(k + i) * n_ + j] * D_[beta[bi] * n_ + alpha[j]]);
                }
                B[i * m + bi] = 2 * b + OP[i * m + bi];
            }    // for bi
        }        // for i

        // final cost
        int lb = compute_cost(p, k) + linear_assignment_cost(B, m);

        return lb;
        // return 0;
    }    // compute_lower_bound

    static qap_task calc_lower_bound(qap_task t, int level) {
        int lb = compute_lower_bound(t.p_, level);
        t.cost = lb;
        return t;
    }

    static qap_task calc_compute_cost(qap_task t) {
        int cc = compute_cost(t.p_);
        t.cost = cc;
        return t;
    }

    hpx::future<qap_task> process(std::vector<int> &p_) {
        std::cout << "current level is : " << level << std::endl;
        hpx::future<qap_task> final_step_val;
        if (level == n_ - 1) {
            std::cout << "in final step" << std::endl;
            // qap_task t;
            // t.p_ = p_;
            // for(int i =0;i<sizeof(p_);i++) {
            //     std::cout << t.p_[i] << " ";
            // }
            // std::swap(t.p_[level], t.p_[n_ - 1]);
            // final_step_val = hpx::async(calc_compute_cost, t);
            // std::cout << "last step" << std::endl;
            // for(int i = 0;i<size(t.p_);i++) {
            //     std::cout <<"elem is" <<  t.p_[i] << " ";
            // }
        } else if(level < n_ - 1) {
            for (int i = level; i < n_; i++) {
                qap_task t;
                t.p_ = p_;
                std::swap(t.p_[level], t.p_[i]);
                tasks_in_step.push_back(hpx::async(calc_lower_bound, t, level));
            }
            final_step_val = hpx::when_all(tasks_in_step).then([&](auto&& f) {
                int step_min_cost = 32000;
                qap_task min_in_step;
                std::vector<hpx::future<qap_task>> results = f.get();
                for (int i = 0; i < results.size(); ++i)
                {
                    qap_task task_val = results[i].get();
                    std::cout << std::endl << "task val is " << task_val.cost << " " << "task permutation is : ";
                    for(int i = 0;i<size(task_val.p_);i++) {
                        std::cout << task_val.p_[i] << " ";
                    }
                    if (task_val.cost < step_min_cost) {
                        min_in_step = task_val;
                        step_min_cost = min_in_step.cost;
                    }
                }
                // once we got the min in step, we need to further generate tasks for this
                // min task. therefore we create another step and start processing it
                // the level should be incremeneted
                std::cout << "level is " << level << " next level is : " << level + 1;
                // step_exec nextStep(level+1);
                // return nextStep.process(min_in_step.p_);
                return min_in_step;
            });
        }
        return final_step_val;
    }
};

int main(int argc, char* argv[])
{
 
    if (read_qaplib_instance(argv[1], step_exec::n_, step_exec::F_, step_exec::D_))
    {
        int i = 0;
        std::vector<int> P0(step_exec::n_);
        std::iota(std::begin(P0), std::end(P0), 0);
        std::vector<hpx::future<qap_task>> step_results;

        // generate steps which compute lb for various permutations
        while (i < step_exec::n_) {
            step_exec step(i);
            step_results.push_back(step.process(P0));
            i++;
        }

        // // barrier
        // hpx::wait_all(step_results);
        // std::cout << "print";
        // // Found out the least of all futures
        // int min_cost = 32000;
        // qap_task min_step;
        // for (int i = 0; i < step_results.size(); ++i)
        // {
        //     auto step_result = step_results[i].get();
        //     if (step_result.cost < min_cost)
        //     {
        //         min_step = step_result;
        //         min_cost = min_step.cost;
        //     }
        // }

        // std::cout << "Min cost" << min_step.cost << std::endl;
        // std::cout << "min permutation: ";
        // for (int j = 0; j < min_step.p_.size(); j++)
        // {
        //     std::cout << min_step.p_.at(j) << ' ';
        // }
    }
    else {
        std::cout << "Error in reading data" << std::endl;
    }

    return hpx::finalize();
}
