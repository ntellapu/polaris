// Including 'hpx/hpx_main.hpp' instead of the usual 'hpx/hpx_init.hpp' enables
// to use the plain C-main below as the direct main HPX entry point.
#include <hpx/modules/timing.hpp>
#include <hpx/future.hpp>
#include <hpx/hpx_main.hpp>
#include <hpx/iostream.hpp>
#include "libhungarian/hungarian.c"
#include "libhungarian/hungarian.hpp"
#include "qap_common.hpp"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <istream>
#include <limits>
#include <numeric>
#include <ostream>
#include <string>
#include <vector>

class qap_task
{
public:
    std::vector<int> p_;
    int cost;
    int level;

    qap_task() = default;
    qap_task(int i, std::vector<int> p0) : level(i), p_(p0)  {};

};

class step_exec
{
public:
    inline static int n_;                 // number of facilities/locations
    inline static std::vector<int> F_;    // flow matrix
    inline static std::vector<int> D_;    // distance matrix
    
    step_exec() = default;

    static int compute_cost(const std::vector<int>& p, int k = n_) {
        int Z = 0;

        for (int i = 0; i < k; ++i)
        {
            for (int j = 0; j < k; ++j)
                Z += F_[i * n_ + j] * D_[p[i] * n_ + p[j]];
        }    // for i

        return Z;
    } // compute_cost

    static std::vector<int> ordered_product(std::vector<int>& F, std::vector<int>& D, int m) {
        std::vector<int> OP(m * m, 0);

        for (auto iter = std::begin(F), end = std::end(F); iter < end;
             iter += m)
        {
            std::sort(iter, iter + m);
        }

        // D must be transposed
        for (auto iter = std::begin(D), end = std::end(D); iter < end;
             iter += m)
        {
            std::sort(iter, iter + m, std::greater<void>());
        }

        for (int i = 0; i < m; ++i)
        {
            for (int j = 0; j < m; ++j)
            {
                // we avoid diagonal elements
                for (int k = 1; k < m; ++k)
                    OP[i * m + j] += (F[i * m + k] * D[j * m + k - 1]);
            }
        }

        return OP;
    }    // ordered_product

    static int compute_lower_bound(const std::vector<int>& p, int k)
    {
        auto alpha = p.data();       // k finished assignments
        auto beta = p.data() + k;    // m assignments to make

        int m = n_ - k;

        // construct linear assignment matrix (Eq. 4)
        std::vector<int> B(m * m, 0);

        // remaining assignments
        std::vector<int> Fp(m * m, 0);
        std::vector<int> Dp(m * m, 0);    // Dp is transposed

        for (int i = 0; i < m; ++i)
        {
            for (int j = 0; j < m; ++j)
                Fp[i * m + j] = F_[(k + i) * n_ + k + j];
        }

        for (int i = 0; i < m; ++i)
        {
            for (int j = 0; j < m; ++j)
                Dp[i * m + j] = D_[beta[j] * n_ + beta[i]];
        }

        auto OP = ordered_product(Fp, Dp, m);

        // assignments made
        for (int i = 0; i < m; ++i)
        {
            for (int bi = 0; bi < m; ++bi)
            {
                int b = 0;
                for (int j = 0; j < k; ++j)
                {
                    b += (F_[(k + i) * n_ + j] * D_[beta[bi] * n_ + alpha[j]]);
                }
                B[i * m + bi] = 2 * b + OP[i * m + bi];
            }    // for bi
        }        // for i

        // final cost
        int lb = compute_cost(p, k) + linear_assignment_cost(B, m);

        return lb;
        // return 0;
    }    // compute_lower_bound

    static qap_task calc_lower_bound(qap_task t) {
        int lb = compute_lower_bound(t.p_, t.level);
        t.cost = lb;
        return t;
    }   // calc_lower_bound

    static qap_task calc_compute_cost(qap_task t) {
        int cc = compute_cost(t.p_);
        t.cost = cc;
        return t;
    }   //calc_compute_cost

    hpx::future<qap_task> process(qap_task curr_task) { 
        std::vector<hpx::future<qap_task>> tasks_in_step;
        hpx::future<qap_task> final_step_val;
        if (curr_task.level == n_ - 1) {

            final_step_val = hpx::async(calc_compute_cost, curr_task);

        } 
        else {                                                              
            for (int i = curr_task.level; i < n_; ++i) {
                qap_task t(curr_task.level, curr_task.p_);
                std::swap(t.p_[t.level], t.p_[i]);
                // std::cout<<"\nCREATION of P : ";
                // for(auto e:t.p_) {
                //     std::cout << e << " ";
                // }
                // std::cout<<std::endl;

                tasks_in_step.push_back(hpx::async(calc_lower_bound, t));
            }
            final_step_val = hpx::when_all(tasks_in_step).then([&](auto&& f) {
                qap_task min_in_step;
                min_in_step.cost = 32000;
                std::vector<hpx::future<qap_task>> results = f.get();
                for (auto& e:results)
                {
                    qap_task task_val = e.get();
                    
                    task_val.level+=1;
                    auto task_val_branch = process(task_val).get();

                    // print
                    // std::cout << std::endl << "Step "  << " task val is " << task_val.cost << " " << "task permutation is : " ;
                    // for(auto e:task_val.p_) {
                    //     std::cout << e << " ";
                    // }

                    // make a future<vector> of task branches 
                    // check min after the for loop. 
                    // That way each branch would have launched async and started explore- won't be getted 
                    // immedietly (currently emulates sequential sync code)


                    // update step_min
                    if (task_val_branch.cost < min_in_step.cost) {
                        min_in_step = task_val_branch;        
                    }
                    
                }                
                return min_in_step;
            });
        }
        return final_step_val;
    }
};

int main(int argc, char* argv[])
{
 
    if (read_qaplib_instance(argv[1], step_exec::n_, step_exec::F_, step_exec::D_))
    {
        int i = 0;
        std::vector<int> P0(step_exec::n_);
        std::iota(std::begin(P0), std::end(P0), 0);

        std::vector<hpx::future<qap_task>> step_results;

        // start timing
        auto start = std::chrono::steady_clock::now();
        
        // generate steps which compute lb for various permutations
        // while (i < step_exec::n_) {
        //     qap_task initial(i, P0);
        //     step_exec step;
        //     step_results.push_back(step.process(initial)); // check if intial perm is considered
        //     i++;
        // }

        // std::cout << "\n-------------------------------\n";
        qap_task initial(0, P0);
        step_exec step1;
        step_results.push_back(step1.process(initial)); // check if intial perm is considered

        // barrier
        // when all can be added -----------
        hpx::wait_all(step_results);
        std::cout << "\nBarrier End\n";
        // Found out the least of all futures
        int min_cost = 32000;
        qap_task min_step;
        for (int i = 0; i < step_results.size(); ++i)
        {
            auto step_result = step_results[i].get();

            std::cout << "MIN for STEP "<< i <<" is "<< step_result.cost <<std::endl;
            std::cout << "PERM for STEP "<< i <<" is ";
            for (int j = 0; j < step_result.p_.size(); j++)
            {
                std::cout <<step_result.p_.at(j) << ' ';
            }

            if (step_result.cost < min_cost)
            {
                min_step = step_result;
                min_cost = min_step.cost;
            }
        }

        std::cout << "\nMin cost : " << min_step.cost << std::endl;
        std::cout << "Min Solution : ";
        for (int j = 0; j < min_step.p_.size(); j++)
        {
            std::cout << min_step.p_.at(j) << ' ';
        }
        std::cout<<std::endl;


        // End Timing
        auto end = std::chrono::steady_clock::now();
        std::chrono::duration<double> time_span = end-start;
        std::cout<< "\nTime :" << time_span.count() << std::endl;

        
    }
    else {
        std::cout << "Error in reading data" << std::endl;
    }

    return hpx::finalize();
}
